public class Survey
	{
		//Surveys are made up of lists lists of strings which are edited and then compiled
		//These lists are then turned into files that are line by line interpretations of the lists of lists
		//END and ENDANSWERS are included to specify to the system when a question ends so it can be read back in
		
		protected List<List<String>> Survey = new ArrayList<List<String>>();
		protected ArrayList<String> Question = new ArrayList<String>();
		protected ArrayList<String> Answer = new ArrayList<String>();
		protected String menuOptionCreation = "1";
		protected Scanner s = new Scanner(System.in);

		
		public Survey()
		{}
		public void createSurvey()
		{
			Survey = new ArrayList<List<String>>();
			Question = new ArrayList<String>();
			menuOptionCreation = "1";
			while(menuOptionCreation.charAt(0) != '2')
			{
				
				System.out.println("1) Add a new T/F question\n2) Add a new multiple choice question\n3) Add a new short answer question\n4) Add a new essay question\n5) Add a new ranking question\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());	
				while(menuOptionCreation.charAt(0) != '1' && menuOptionCreation.charAt(0) != '2' && menuOptionCreation.charAt(0) != '3' && menuOptionCreation.charAt(0) != '4' && menuOptionCreation.charAt(0) != '5')
				{
				System.out.println("Invalid Menu Option please try again");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				}
				if(menuOptionCreation.charAt(0) == '1')
				{
					Question.add("tf");
				}
				else if(menuOptionCreation.charAt(0) == '2')
				{
					Question.add("mc");
				}
				else if(menuOptionCreation.charAt(0) == '3')
				{
					Question.add("sa");
				}
				else if(menuOptionCreation.charAt(0) == '4')
				{
					Question.add("es");
				}
				else if(menuOptionCreation.charAt(0) == '5')
				{
					Question.add("ranking");
				}
				System.out.println("Please input prompt\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				Question.add(menuOptionCreation);
				
				if( (Question.get(0)) != "tf" && (Question.get(0)) != "sa" && (Question.get(0)) != "es" )
				{
					System.out.println("Enter first/only answer\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				Question.add(menuOptionCreation);
					System.out.println("1) Add answer\n2) Done adding Answers\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				while(menuOptionCreation.charAt(0) != '1' && menuOptionCreation.charAt(0) != '2')
					{
						System.out.println("Improper Input please try again\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				}
			
			
					while(menuOptionCreation.charAt(0) == '1')
					{			
						System.out.println("Enter new Answer\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				Question.add(menuOptionCreation);
						System.out.println("1) Add answer\n2) Done adding Answers\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				while(menuOptionCreation.charAt(0) != '1' && menuOptionCreation.charAt(0) != '2')
						{
							System.out.println("Improper Input please try again\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				}
					}
				}
				if((Question.get(0)) == "tf")
				{
					Question.add("true");
					Question.add("false");					
				}
				Question.add("END");
				Survey.add(Question);
				Question = new ArrayList<String>();
				System.out.println("Would you like to add another question?\n1)Yes\n2)No");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				while(menuOptionCreation.charAt(0) != '1' && menuOptionCreation.charAt(0) != '2')
					{
						System.out.println("Improper Input please try again\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				}
			}
		}
		public void displaySurvey()
		{
			if(Survey.isEmpty())
			{
				System.out.println("No survey currently loaded");
			}
			else
			{
				System.out.println("Currently loaded Survey:\n");
				int i = 2;
				for(int j = 0; j< Survey.size(); j++)
				{
					i = 2;
					System.out.println(Survey.get(j).get(1)+"?");
					if(Survey.get(j).get(0) != "es" && Survey.get(j).get(0) != "sa")
					{
						
						while (!Survey.get(j).get(i).equals("END"))
						{
							System.out.println(i + ") " + Survey.get(j).get(i));
							i++;
						}
						System.out.println("\n");
					} 
				}
			}
		}
		public void saveSurvey(String filename,String testSurvey)
		{
			if(Survey.isEmpty())
			{
				System.out.println("No survey currently loaded");
			}
			else
			{

				try (PrintWriter writer = new PrintWriter(filename+"."+testSurvey, "UTF-8")){
				for(int j = 0; j< Survey.size(); j++)
				{
					for (int i = 0; i<Survey.get(j).size();i++)
						writer.println(Survey.get(j).get(i));
				}
				
				System.out.println("File successfuly saved");
				}
				catch(IOException e)
				{
					System.out.println("File could not be created");
				}
			}
		}
		public String loadSurvey()
			{				
				String filenameFinal = "";
				File folder = new File("./");
				File[] listOfFiles = folder.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith("surv");
				}});
				
				
				int numberOfFiles = 0;
				for (int i = 0; i < listOfFiles.length-2; i++)
				{
					if (listOfFiles[i].isFile()) 
					{
						System.out.println((i+1)+")"+ listOfFiles[i].getName());
					} 
					numberOfFiles++;
				}
				if(numberOfFiles == 0)
				{
					System.out.println("There are no Surveys to load you must create one first");
					System.exit(0);
				}
				
				System.out.println("\nWhich File would you like to load?");
				String loadFile = "";
					do{
					loadFile = s.nextLine();
					}while(loadFile.isEmpty());	
				while(Character.getNumericValue(loadFile.charAt(0)) <= 0 || Character.getNumericValue(loadFile.charAt(0)) > numberOfFiles)
				{
					System.out.println("Invalid File Number");
					do{
					loadFile = s.nextLine();
					}while(loadFile.isEmpty());	
				}
				
				try 
				{
					filenameFinal = listOfFiles[Character.getNumericValue(loadFile.charAt(0))-1].getName();
					File file = new File(listOfFiles[Character.getNumericValue(loadFile.charAt(0))-1].getName());
					FileReader fileReader = new FileReader(file);
					BufferedReader bufferedReader = new BufferedReader(fileReader);
					StringBuffer stringBuffer = new StringBuffer();
					String line;
					while ((line = bufferedReader.readLine()) != null)
					{
						Question.add(line);
						if(line.equals("END"))
						{
							Survey.add(Question);
							Question = new ArrayList<String>();
						}
					}
					fileReader.close();			
				}
				
				catch (IOException e) 
				{
				System.out.println("Unable to load file");
				}
			return filenameFinal;	
			}
		public void modifySurvey(String testSurvey)
			{
				System.out.println("What survey do you wish to modify? (Please include file extension)");
				String fileName = "";
				String loadFile = "";
				while(! loadFile.equals("BACK"))
				{
					do{
					loadFile = s.nextLine();
					}while(loadFile.isEmpty());	
				if(! loadFile.equals("BACK"))
					{
						try 
						{
							File file = new File(loadFile);
							FileReader fileReader = new FileReader(file);
							BufferedReader bufferedReader = new BufferedReader(fileReader);
							StringBuffer stringBuffer = new StringBuffer();
							String line;
							while ((line = bufferedReader.readLine()) != null)
							{
								Question.add(line);
								if(line.equals("END"))
								{
									Survey.add(Question);
									Question = new ArrayList<String>();
								}
							}
							fileReader.close();	
							fileName = loadFile;
							loadFile = "BACK";
							
						}
						catch (IOException e) 
						{
						System.out.println("Unable to load file\nEnter Valid File or 'BACK'");
						}
					}
				}
				String Menu = "";
				int MenuOpt = -1;
					System.out.println("What question number do you wish to modify? (there are "+ Survey.size() + " Questions) \nInput 'BACK' at any time to finish editing.");
				do{
				Menu = s.nextLine();
				}while(Menu.isEmpty());
			while(! Menu.equals("BACK") )
				{
					try
					{
					MenuOpt = Integer.parseInt(Menu); 
					}
					catch(NumberFormatException e)
					{
						System.out.println("Invalid Question Number");
					}
					if(MenuOpt > 0 && MenuOpt <= Survey.size())
					{
						Survey.set(MenuOpt-1,modifySurveyQuestion(Survey.get(MenuOpt-1),1));
						
							System.out.println("Enter new filename (omit the file extension)");
							do{
							fileName = s.nextLine();
							}while(fileName.isEmpty());
							
						saveSurvey(fileName,testSurvey);
						Menu = "BACK";

					}
					else
					{
						System.out.println("Invalid Question Number");
					}
					System.out.println("What question do you wish to modify?\nInput 'BACK' at any time to finish editing.");
					do{
					Menu = s.nextLine();
					}while(Menu.isEmpty());	
				}

			}
			public List<String> modifySurveyQuestion(List<String> editQ,int surveyTest)
			{
				String finalMenu = "0";
				int MenuOpt = -1;
				String menuSelect = "0";
				String editQStr = "";
				System.out.println("Would you like to edit the prompt?\n1)Yes\n2)No");
				while(menuSelect.charAt(0) != '1' && menuSelect.charAt(0) != '2')
				{
					do{
					menuSelect = s.nextLine();
					}while(menuSelect.isEmpty());		
				if(menuSelect.charAt(0) == '1')
					{
						System.out.println(editQ.get(1)+"\nEnter a new Prompt:");
						do{
						editQStr = s.nextLine();
						}while(editQStr.isEmpty());
						editQ.set(1,editQStr);
					}
				}
				if(editQ.size() > 3)
				{
				menuSelect = "0";
				System.out.println("Would you like to edit the choices?\n1)Yes\n2)No");
				while(menuSelect.charAt(0) != '1' && menuSelect.charAt(0) != '2')
				{
						do{
						menuSelect = s.nextLine();
						}while(menuSelect.isEmpty());
					if(menuSelect.charAt(0) == '1')
					{
						while(! (finalMenu.charAt(0) == '2'))
						{
						System.out.println("Enter Which option you would like to change");
						for(int i = 2; i<editQ.size()-surveyTest; i++)
						{
							System.out.println((i-1) + ") " + editQ.get(i));
						}
						MenuOpt = -1;
						while(MenuOpt < 0 || MenuOpt >= (editQ.size() - surveyTest -1))
						{
						do{
						menuSelect = s.nextLine();
						}while(menuSelect.isEmpty());
							try
							{
							MenuOpt = Integer.parseInt(menuSelect); 
							}
							catch(NumberFormatException e)
							{
								System.out.println("Invalid Question Number");
							}
							System.out.println("Invalid Question Number");
						}
						System.out.println("Enter new Value");
						do{
						editQStr = s.nextLine();
						}while(editQStr.isEmpty());
						editQ.set(MenuOpt+1,editQStr);
						MenuOpt = -1;
						System.out.println("Would you like to change another answer\n1)Yes\n2)No");
						do{
						finalMenu = s.nextLine();
						}while(finalMenu.isEmpty());
					while(finalMenu.charAt(0) != '1' && finalMenu.charAt(0) != '2')
						{
							System.out.println("Incorrect Input");
						do{
						finalMenu = s.nextLine();
						}while(finalMenu.isEmpty());
					}
						}
					}

				}				
				}
				return editQ;
			}
		public void takeSurvey(String filename)
		{
			
			System.out.println("What is your name?");
			Answer.add(s.nextLine());
			for(int i = 0; i<= Survey.size()-1; i++)
			{
				System.out.println(Survey.get(i).get(1)+"?");
				if(Survey.get(i).get(0).equals("tf"))
				{
					System.out.println("1)True\n2)False");
				}
				else if(Survey.get(i).get(0).equals("mc"))
				{
					System.out.println("If more than one apply enter all answers e.g. if 1 and 3 are right enter '1 3'");
					for(int j = 2; j<Survey.get(i).size()-1; j++)
					{
						System.out.println(j-1+")"+Survey.get(i).get(j));
					}
				}
				else if(Survey.get(i).get(0).equals("ranking"))
				{
					System.out.println("Rank all in the order they appear: e.g. if the first option is 2 and second option is 1 enter '2 1'");
					for(int j = 2; j<Survey.get(i).size()-1; j++)
					{
						System.out.println(j-1+")"+Survey.get(i).get(j));
					}
				}
				Answer.add(s.nextLine());
			}
			try (PrintWriter writer = new PrintWriter(filename+"-"+Answer.get(0)+".surv-Resp", "UTF-8")){
				for(int i = 0; i< Answer.size(); i++)
				{
						writer.println(Answer.get(i));
				}
				
				System.out.println("File successfuly saved");
				}
				catch(IOException e)
				{
					System.out.println("File could not be created");
				}
		}
		public void tabulateSurvey()
			{
				List<List<String>> multiResponses = new ArrayList<List<String>>();
				List<List<String>> sortedSurveyResponses = new ArrayList<List<String>>();
				ArrayList<String> singleResponses = new ArrayList<String>();
				
				String Menu = "";
				int MenuOpt = -1;
				System.out.println("Choose a Survey to Tabulate");
				File folder = new File("./");
				File[] listOfFiles = folder.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
				return name.endsWith("surv");
				}});
				int numberOfFiles = 0;
				for (int i = 0; i < listOfFiles.length-2; i++)
				{
					if (listOfFiles[i].isFile()) 
					{
						System.out.println((i+1)+")"+ listOfFiles[i].getName());
					} 
					numberOfFiles++;
				}
				if(numberOfFiles > 0)
				{
					
				
				while(MenuOpt < 0)
				{
				do{
				Menu = s.nextLine();
				}while(Menu.isEmpty());
				{
					try
					{
					MenuOpt = Integer.parseInt(Menu); 
					}
					catch(NumberFormatException e)
					{
						System.out.println("Invalid Question Number");
					}
				}
				String startsWith = listOfFiles[MenuOpt-1].getName();
				System.out.println(startsWith);
				folder = new File("./");
				listOfFiles = folder.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
				return name.startsWith(startsWith);
				}});
				numberOfFiles = 0;
				for (int i = 1; i < listOfFiles.length; i++)
				{
					if (listOfFiles[i].isFile()) 
					{
						try 
					{
						File file = new File(listOfFiles[i].getName());
						FileReader fileReader = new FileReader(file);
						BufferedReader bufferedReader = new BufferedReader(fileReader);
						StringBuffer stringBuffer = new StringBuffer();
						String line;
						while ((line = bufferedReader.readLine()) != null)
						{
						singleResponses.add(line);
						}
						multiResponses.add(singleResponses);
						singleResponses = new ArrayList<String>();
						
						for(int j = 0; j<multiResponses.size()-1; j++)
						{
							sortedSurveyResponses.add(new ArrayList<String>());
						}
					}
					catch (IOException e) 
					{
					System.out.println("Unable to load file");
					}
					} 
					numberOfFiles++;
				}
				for(int j = 0; j<multiResponses.size(); j++){
					for(int i = 1; i<multiResponses.get(0).size(); i++)
					{
						sortedSurveyResponses.get(i-1).add(multiResponses.get(j).get(i));
					}
				}
				for(int i = 0; i<sortedSurveyResponses.size()-1; i++)
				{
					System.out.println("Responses for Question " + (i+1) + ":\n" + sortedSurveyResponses.get(i));

				}
				
				
			}}}
		
		
	}