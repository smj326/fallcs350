public class Test extends Survey
	{

		public Test()
		{}
		public void addAnswers()
		{
			System.out.println(Survey.size());
			for(int j = 0; j< Survey.size(); j++)
			{
				if(Survey.get(j).get(0) == "es")
				{
					Survey.get(j).add("ESSAY");
					Survey.get(j).add("ENDANSWERS");
				}
				else
				{
					System.out.println("Question Type: "+Survey.get(j).get(0));
					System.out.println("Prompt: " + Survey.get(j).get(1));
					System.out.println("Possible Answers:");
					for(int i = 2; i<Survey.get(j).size()-1; i++)
					{
						System.out.println(i-1+") " +Survey.get(j).get(i));
					}
					String properInput = "0";
					while(properInput.charAt(0) != '1')
					{
						System.out.println("Please Input the correct answer/answers corresponding number/s\nif the correct answer for a multiple choice is 1 2 and 3 enter '123'\nif it is ranking and the correct rank of each answer in the order it's\nlisted is 4 3 2 1 enter '4321'\nIf it is short answer just type the correct word/sentence");
						do{
						menuOptionCreation = s.nextLine();
						}while(menuOptionCreation=="");
						System.out.println("Is " + menuOptionCreation + " correct for your input?\n1)Yes\n2)No, I want to re-enter the correct answer");
						do{
						properInput = s.nextLine();
						}while(properInput.isEmpty());	
					while(properInput.charAt(0) != '1' && properInput.charAt(0) != '2')
								{
									System.out.println("Improper Input please try again\n");
										do{
										properInput = s.nextLine();
										}while(properInput.isEmpty());			
									}
					}	
					Survey.get(j).add(menuOptionCreation.toLowerCase());
					Survey.get(j).add("ENDANSWERS");
				}			
		}		
	}
	public void displayTest()
		{
			if(Survey.isEmpty())
			{
				System.out.println("No survey currently loaded");
			}
			else
			{
				System.out.println("Currently loaded Survey:\n");
				int i = 2;
				for(int j = 0; j< Survey.size(); j++)
				{
					i = 2;
					System.out.println(Survey.get(j).get(1)+"?");
					if(Survey.get(j).get(0) != "es" && Survey.get(j).get(0) != "sa")
					{
						
						while (!Survey.get(j).get(i).equals("END"))
						{
							System.out.println(i + ") " + Survey.get(j).get(i));
							i++;
						}
						System.out.println("The correct answer is: "+ Survey.get(j).get(i+1));
						System.out.println("\n");
					} 
				}
			}
		}
		public String loadTest()
			{				
				String filenameFinal = "";
				File folder = new File("./");
				File[] listOfFiles = folder.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith("test");
				}});
				int numberOfFiles = 0;
				for (int i = 0; i < listOfFiles.length; i++)
				{
					if (listOfFiles[i].isFile()) 
					{
						System.out.println((i+1)+")"+ listOfFiles[i].getName());
					} 
					numberOfFiles++;
				}
				if(numberOfFiles == 0)
					return "";
				System.out.println("\nWhich File would you like to load?");
				String loadFile = "";
				do{
				loadFile = s.nextLine();
				}while(loadFile.isEmpty());	
			while(Character.getNumericValue(loadFile.charAt(0)) <= 0 || Character.getNumericValue(loadFile.charAt(0)) > numberOfFiles)
				{
					System.out.println("Invalid File Number");
					do{
					loadFile = s.nextLine();
					}while(loadFile.isEmpty());				}
				
				try 
				{
					filenameFinal = listOfFiles[Character.getNumericValue(loadFile.charAt(0))-1].getName();
					File file = new File(listOfFiles[Character.getNumericValue(loadFile.charAt(0))-1].getName());
					FileReader fileReader = new FileReader(file);
					BufferedReader bufferedReader = new BufferedReader(fileReader);
					StringBuffer stringBuffer = new StringBuffer();
					String line;
					while ((line = bufferedReader.readLine()) != null)
					{
						if(!line.equals(""))
						{
							Question.add(line);
							if(line.equals("ENDANSWERS"))
							{
								Survey.add(Question);
								Question = new ArrayList<String>();
							}
						}
					}
					fileReader.close();
				}
				catch (IOException e) 
				{
				System.out.println("Unable to load file");
				}
				return filenameFinal;
			}
			
			public void takeTest(String filename)
		{
			System.out.println("What is your name?");
			Answer.add(s.nextLine());
			for(int i = 0; i<= Survey.size()-1; i++)
			{
				System.out.println(Survey.get(i).get(1)+"?");
				if(Survey.get(i).get(0).equals("tf"))
				{
					System.out.println("1)True\n2)False");
				}
				else if(Survey.get(i).get(0).equals("mc"))
				{
					System.out.println("If more than one apply enter all answers e.g. if 1 and 3 are right enter '1 3'");
					for(int j = 2; j<Survey.get(i).size()-3; j++)
					{
						System.out.println(j-1+")"+Survey.get(i).get(j));
					}
				}
				else if(Survey.get(i).get(0).equals("ranking"))
				{
					System.out.println("Rank all in the order they appear: e.g. if the first option is 2 and second option is 1 enter '2 1'");
					for(int j = 2; j<Survey.get(i).size()-3; j++)
					{
						System.out.println(j-1+")"+Survey.get(i).get(j));
					}
				}
				Answer.add(s.nextLine());
			}
			try (PrintWriter writer = new PrintWriter(filename+"-"+Answer.get(0)+".test-Resp", "UTF-8")){
				for(int i = 0; i< Answer.size(); i++)
				{
						writer.println(Answer.get(i));
				}
				
				System.out.println("File successfuly saved");
				}
				catch(IOException e)
				{
					System.out.println("File could not be created");
				}
		}
		
		
		public void modifyTest(String fileName)
			{
				
				String Menu = "";
				String ansStr = "";
				String Answers = "";
				String newAns = "";
				int MenuOpt = -1;
					System.out.println("What question number do you wish to modify? (there are "+ Survey.size() + " Questions) \nInput 'BACK' at any time to finish editing.");
				do{
				Menu = s.nextLine();
				}while(Menu.isEmpty());
			while(! Menu.equals("BACK") )
				{
					try
					{
					MenuOpt = Integer.parseInt(Menu); 
					}
					catch(NumberFormatException e)
					{
						System.out.println("Invalid Question Number");
					}
					if(MenuOpt > 0 && MenuOpt <= Survey.size())
					{
						int beginAnswers = 0;
						Survey.set(MenuOpt-1,modifySurveyQuestion(Survey.get(MenuOpt-1),3));
						System.out.println("Would you like to modify the correct Answers?\n1)Yes\n2)No");
						do{
						Menu = s.nextLine();
						}while(Menu.isEmpty());
						if(Menu.charAt(0) == '1')
						{
							System.out.println("Which Question, there are " + Survey.size());
							MenuOpt = -1;
							while(MenuOpt <= 0 || MenuOpt > Survey.size())
							{
							Menu = "";
							do{
							Menu = s.nextLine();
							}while(Menu.isEmpty());
							
							try
							{
							MenuOpt = Integer.parseInt(Menu); 
							}
							catch(NumberFormatException e)
							{
						System.out.println("Invalid Question Number");
							}
							if(MenuOpt <= 0 || MenuOpt > Survey.size())
							{
							System.out.println("Invalid Question Number Enter a new Number");
							}
							}
								System.out.println("This is the question as is with the listed answers followed by the listed correct answer");
								for(int i = 1; i< Survey.get(MenuOpt-1).size()-3; i++)
								{
									System.out.println(Survey.get(MenuOpt-1).get(i));
								}
								System.out.println(Survey.get(MenuOpt-1).get(Survey.get(MenuOpt-1).size()-2));
								System.out.println("Please Enter new Answer or press enter to leave unedited");
								newAns = s.nextLine();
								if(!newAns.isEmpty())
								{
									Survey.get(MenuOpt-1).set(Survey.get(MenuOpt-1).size()-2,newAns);
								}							
						}
							System.out.println("Enter new filename (omit the file extension)");
							do{
							fileName = s.nextLine();
							}while(fileName.isEmpty());	
								
						saveSurvey(fileName,"test");
						Menu = "BACK";
					}
					else
					{
						System.out.println("Invalid Question Number");
					}
					System.out.println("What question do you wish to modify?\nInput 'BACK' at any time to finish editing.");
					do{
					Menu = s.nextLine();
					}while(Menu.isEmpty());	
				}

			}
			
			public void tabulateTest()
			{
				List<List<String>> multiResponses = new ArrayList<List<String>>();
				List<List<String>> sortedSurveyResponses = new ArrayList<List<String>>();
				ArrayList<String> singleResponses = new ArrayList<String>();
				
				String Menu = "";
				int MenuOpt = -1;
				System.out.println("Choose a Test to Tabulate");
				File folder = new File("./");
				File[] listOfFiles = folder.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
				return name.endsWith("test");
				}});
				int numberOfFiles = 0;
				for (int i = 0; i < listOfFiles.length; i++)
				{
					if (listOfFiles[i].isFile()) 
					{
						System.out.println((i+1)+")"+ listOfFiles[i].getName());
					} 
					numberOfFiles++;
				}
				if(numberOfFiles > 0)
				{
					
				
				while(MenuOpt < 0)
				{
				do{
				Menu = s.nextLine();
				}while(Menu.isEmpty());
				{
					try
					{
					MenuOpt = Integer.parseInt(Menu); 
					}
					catch(NumberFormatException e)
					{
						System.out.println("Invalid Question Number");
					}
				}
				String startsWith = listOfFiles[MenuOpt-1].getName();
				System.out.println(startsWith);
				folder = new File("./");
				listOfFiles = folder.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
				return name.startsWith(startsWith);
				}});
				numberOfFiles = 0;
				for (int i = 1; i < listOfFiles.length; i++)
				{
					if (listOfFiles[i].isFile()) 
					{
						try 
					{
						File file = new File(listOfFiles[i].getName());
						FileReader fileReader = new FileReader(file);
						BufferedReader bufferedReader = new BufferedReader(fileReader);
						StringBuffer stringBuffer = new StringBuffer();
						String line;
						while ((line = bufferedReader.readLine()) != null)
						{
						singleResponses.add(line);
						}
						multiResponses.add(singleResponses);
						singleResponses = new ArrayList<String>();
						
						for(int j = 0; j<multiResponses.size()-1; j++)
						{
							sortedSurveyResponses.add(new ArrayList<String>());
						}
					}
					catch (IOException e) 
					{
					System.out.println("Unable to load file");
					}
					} 
					numberOfFiles++;
				}
				for(int j = 0; j<multiResponses.size(); j++){
					for(int i = 1; i<multiResponses.get(0).size(); i++)
					{
						sortedSurveyResponses.get(i-1).add(multiResponses.get(j).get(i));
					}
				}
				for(int i = 0; i<sortedSurveyResponses.size(); i++)
				{
					System.out.println("Responses for Question " + (i+1) + ":\n" + sortedSurveyResponses.get(i));

				}
				
				}
			}}
		
			public void gradeTest()
			{
				String name = "";
				int score = 0;
				ArrayList<String> Answers = new ArrayList<String>();
				ArrayList<String> answerKey = new ArrayList<String>();

				System.out.println("Choose a Response to Grade");
				String filenameFinal = "";
				File folder = new File("./");
				File[] listOfFiles = folder.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
				return name.endsWith("test-Resp");
				}});
				int numberOfFiles = 0;
				for (int i = 0; i < listOfFiles.length; i++)
				{
					if (listOfFiles[i].isFile()) 
					{
						System.out.println((i+1)+")"+ listOfFiles[i].getName());
					} 
					numberOfFiles++;
				}
				if(numberOfFiles != 0)
				{
					System.out.println("\nWhich File would you like to load?");
					String loadFile = "";
					do{
					loadFile = s.nextLine();
					}while(loadFile.isEmpty());	
					
				while(Character.getNumericValue(loadFile.charAt(0)) <= 0 || Character.getNumericValue(loadFile.charAt(0)) > numberOfFiles)
					{
						System.out.println("Invalid File Number");
						do{
						loadFile = s.nextLine();
						}while(loadFile.isEmpty());				}	
					try 
					{
						filenameFinal = listOfFiles[Character.getNumericValue(loadFile.charAt(0))-1].getName();
						File file = new File(listOfFiles[Character.getNumericValue(loadFile.charAt(0))-1].getName());
						FileReader fileReader = new FileReader(file);
						BufferedReader bufferedReader = new BufferedReader(fileReader);
						StringBuffer stringBuffer = new StringBuffer();
						String line;
						while ((line = bufferedReader.readLine()) != null)
						{
						Answers.add(line);
						}
					}
					catch (IOException e) 
					{
					System.out.println("Unable to load file");
					}
					for(int i = 0; i<= Survey.size()-1; i++)
					{
						answerKey.add(Survey.get(i).get(Survey.get(i).size()-2));
					}
					name = Answers.get(0);
					Answers.remove(0);
					for(int i = 0; i< answerKey.size(); i++)
					{
						if(!answerKey.get(i).equals("ESSAY"))
						{
							if(Answers.get(i).toLowerCase().replaceAll(" ", "").replaceAll("\n", "").equals(answerKey.get(i).toLowerCase().replaceAll(" ", "").replaceAll("\n", "")))
							{
								score += 10;
							};
						}
					}
					System.out.println(name + " got " + score + " / " + answerKey.size()*10 + "\nDon't forget to go grade the essay questions if there are any!");
				}
				else
				{
					System.out.println("There are no responses to grade in the current directory");
				}
			}
			
			

}
}