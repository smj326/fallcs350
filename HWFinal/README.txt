Sean Jamison
Drexel University
CS350 HW3/4
smj326@drexel.edu


The program works for all specifications given. To run it just run make in the directory with the java files
A sample test and survey file are given

There is one issue I could not resolve, when a test or survey is taken for its first time it runs the method twice so the answer sheet is recorded incorrectly and has to be thrown out.
It is not a parent/child class issue because it happens in each and I tried seperating the methods to prevent it but it still happens and only sometimes.

I do not believe any crashes exist, or I did not run into them.

There are tests and responses provided for testing purposes.

