Sean Jamison
Drexel University
CS350 Lab 1
smj326@drexel.edu


This demonstrates both part 1 and 2 of lab, but is uploaded to both because I wasn't sure what to do about that.

This program meets the specifications given for the lab, to run it just run make in the directory with the java source file

The demo that is shown on the clock does the following in this order:


Sets Time
Displays Time
Sets and Plays AM radio
turns radio off
Sets and Plays FM radio
turns radio off
sets the volume
sets the alarm
rolls time forward
Buzzer alarm goes off
time rolls forward
snooze is pushed
9 min roll by
Buzzer goes off from the snooze
one minute passes
alarm is turned off
The alarm is reset to use the radio 
10 minutes go by
the alarm turns on to the radio using its last settings FM 104.5
The radio is turnt off