/*************************
Sean Jamison
Drexel University CS350
Lab 1

smj326@drexel.edu
**************************/

public class alarmClock
{
	public static void main(String[] args) {
		
		alarmClock alarm = new alarmClock();
		alarm.start();
	}
	public void start()
	{
		int MINUTES_PER_HOUR = 60;

		Clock myClock = new Clock();
		Alarm myAlarm = new Alarm();
		Radio myRadio = new Radio();
		
		//Driver-Demo Begins here; there is no menu implementation because for this example everything is hardcoded examples
		
		//Set Time and display it
		
		//	Time is in 24 hours within the clock, but is outputed in 12 hr AM/PM, if the menu were implemented it would take in an AM/PM argument
		//	and adjust your input from there to be consistant (this is done because i found it easier to deal with, as well as the fact that
		//	adding implementation for a 24hr clock would be extremely easy)
		myClock.setHours(13);
		myClock.setMinutes(37);
		myClock.displayTime();

		
		//Set Alarm
		//	Same 24 hour deal as Time input
		myAlarm.setAlarmHours(13);
		myAlarm.setAlarmMinutes(43);
		
		//Set AM Station then turn radio on and off again
		myRadio.setAM(673);
		myRadio.playRadio();
		myRadio.stopRadio();
		
		//Switch to FM, set FM station, turn radio on then off
		//	FM stations are dealt with in ints and then the hardware will convert it to a frequency and move the decimal one place
		myRadio.switchAMFM();
		myRadio.setFM(1045);
		myRadio.playRadio();
		myRadio.stopRadio();
		
		//Set Volume
		myRadio.setVolume(14);
		
	
		//Time loop and Alarm Demo
		//	There is a nested loop so it counts seconds, and then I can change the number of hours with a single part of the loop
		
		
		for(int i = 0; i < 10 ; i++)
		{
			for(int j = 0; j<60; j++)
			{
				myClock.tick();
			}
			
			if(myAlarm.alarmTimeCheck(myClock.getMinutes(), myClock.getHours()) && myAlarm.getOnOff())
			{
				int alarmGoOff = myAlarm.switchOnOff();
				
				if (alarmGoOff == 1)
				{
					myRadio.playRadio();
				}
				else if (alarmGoOff == 0)
				{
					myRadio.stopRadio();
				}
				
			}
			
			myClock.displayTime();
			

		}
		//Hit Snooze Button
		
		myAlarm.startSnooze();
		
		for(int i = 0; i < 10 ; i++)
		{
			for(int j = 0; j<60; j++)
			{
				myClock.tick();
			}
			if(myAlarm.getSnoozeSwitch() && myAlarm.getSnooze() > 0)
			{
				myAlarm.setSnooze(myAlarm.getSnooze()-1);
			}
			else if (myAlarm.getSnoozeSwitch() && myAlarm.getSnooze() == 0)
			{
					int alarmGoOff = myAlarm.switchOnOff();
				
					if (alarmGoOff == 1)
					{
						myRadio.playRadio();
					}
					else if (alarmGoOff == 0)
					{
						myRadio.stopRadio();
					}
				myAlarm.switchSnoozeSwitch();
			}
			
			myClock.displayTime();

		}
		//You get up to turn off the alarm
		myAlarm.buzzerOff();
		
		//You switch it to radio mode and set an alarm for your friend that needs to wake up in 10 minutes
		
		myAlarm.switchBuzzRadio();
		myAlarm.setAlarmHours(14);
		myAlarm.setAlarmMinutes(7);
		myAlarm.switchAlarm();
		
		for(int i = 0; i < 11 ; i++)
		{
			for(int j = 0; j<60; j++)
			{
				myClock.tick();
			}

			if(myAlarm.alarmTimeCheck(myClock.getMinutes(), myClock.getHours()) && myAlarm.getOnOff())
			{
				int alarmGoOff = myAlarm.switchOnOff();
				
				if (alarmGoOff == 1)
				{
					myRadio.playRadio();
				}
				else if (alarmGoOff == 0)
				{
					myRadio.stopRadio();
				}
				
			}
			
			myClock.displayTime();

		}
		
		//Your friend turns off the radio and leaves
		myRadio.stopRadio();
		
	}
	public class Clock{
	
			private int seconds = 0;
			private int minutes = 0;
			private int hours = 0;
			private String time = "The current time is ";

			public Clock() 
		{
			seconds = 0;
			minutes = 0;
			hours = 0;
		}
		public int getSeconds()
		{
			return seconds;
		}
		public int getMinutes()
		{
			return minutes;
		}
		public int getHours()
		{
			return hours;
		}
		public void setMinutes(int newVal)
		{
			minutes = newVal;
			seconds = 0;
		}
		public void setHours(int newVal)
		{
			hours = newVal;
		}
		public void tick()
		{
			if(seconds < 59)
			{
				seconds++;
			}
			else if(seconds == 59 && minutes != 59)
			{
				seconds = 0;
				minutes++;
			}
			else if (seconds == 59 && minutes == 59 && hours != 23)
			{
				seconds = 0;
				minutes = 0;
				hours++;
			}
			else if (seconds == 59 && minutes == 59 && hours == 23)
			{
				seconds = 0;
				minutes = 0;
				hours = 0;
			}
		}
		public void displayTime()
		{
			String time = "The current time is ";
			if(hours > 12)
			{
				time += (hours - 12);
				time += ":";
				if(minutes<10)
				{
					time+="0";
				}
				time += (minutes);
				if(hours != 0)
				{
					time += " PM";
				}
				else
				{
					time += " AM";
				}
			}
			else
			{
				if ( hours == 0 )
					time += "12";
				else
				{
				time += hours;
				}
				time += ":";
				if(minutes<10)
				{
					time+="0";
				}
				time += minutes;
				
				time+=" AM";
			}
			time+= "\n";
			System.out.println(time);
		}
		
	}
	public class Alarm{
		
			private int alarmMinutes = 0;
			private int alarmHours = 0;
			private int snooze = 0;
			private boolean onOff;
			private boolean snoozeSwitch;
			private boolean buzzRadio;
			
			public Alarm()
			{
				alarmMinutes = 0;
				alarmHours = 0;
				snooze = 0;
				onOff = false;
				snoozeSwitch = false;
				buzzRadio = false;
			}
			
		public boolean getOnOff()
		{
			return onOff;
		}
		public void shutOffAlarm()
		{
			System.out.println("The alarm is now off\n");
		}
		public void startSnooze()
		{
			System.out.println("Snooze button pushed\n");
			snooze = 9;
			snoozeSwitch = !snoozeSwitch;
		}
		public void switchAlarm()
		{
			onOff = !onOff;
		}
		public int getSnooze()
		{
			return snooze;
		}
		public boolean getSnoozeSwitch()
		{
			return snoozeSwitch;
		}
		public void switchSnoozeSwitch()
		{
			snoozeSwitch = !snoozeSwitch;
		}
		public void setSnooze(int newVal)
		{
			snooze = newVal;
		}
		public boolean getBuzzRadio()
		{
			return buzzRadio;
		}
		public boolean switchBuzzRadio()
		{
			buzzRadio = ! buzzRadio;
			return buzzRadio;
		}
		public int switchOnOff()
		{
			if(onOff){
				if(buzzRadio)
				{
					return 1;
				}
				else
				{
					buzzerOff();
					return -1;
				}
			}
			else
			{
				if(buzzRadio)
				{
					return 0;
				}
				else
				{
					buzzer();
					return -1;
				}
			}
		}
		public int getAlarmMinutes()
		{
			return alarmMinutes;
		}
		public void buzzerOff()
		{
			System.out.println("Buzzer is off\n");
		}
		public int getAlarmHours()
		{
			return alarmHours;
		}
		public void setAlarmMinutes(int newVal)
		{
			alarmMinutes = newVal;
		}
		public void setAlarmHours(int newVal)
		{
			alarmHours = newVal;
		}
		public void buzzer()
		{
			if(!buzzRadio)
			{
				System.out.println("Buzz Buzz Buzz\n");
			}
		}
		public boolean alarmTimeCheck(int min, int hour)
		{
			if(!buzzRadio && min == alarmMinutes && hour == alarmHours)
			{
				buzzer();
				return false;
			}
			else if(buzzRadio && min == alarmMinutes && hour == alarmHours)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	public class Radio{
		
		private int volume;
		private boolean AMFM;
		private int stationAM;
		private int stationFM;
		String radioPlay = "The radio is now playing ";

		
		public Radio()
		{
			volume = 5;
			stationAM = 600;
			stationFM = 1045;
		}
		public int getVolume()
		{
			return volume;
		}
		public void setVolume(int newVal)
		{
			volume = newVal;
			System.out.println("Volume: "+ volume + "\n");
		}
		public int getAM()
		{
			return stationAM;
		}
		public void setAM(int newVal)
		{
			//add error checking
			stationAM = newVal;
		}
		public int getFM()
		{
			return stationFM;
		} 
		public void setFM(int newVal)
		{
			//add error checking
			stationFM = newVal;
		}
		public boolean switchAMFM()
		{
			AMFM = !AMFM;
			return AMFM;
		}
		public boolean getAMFM()
		{
			return AMFM;
		}
		public void playRadio()
		{
			if(AMFM)
			{
				radioPlay += getFM();
				radioPlay = radioPlay.substring(0,radioPlay.length()-1)+"."+radioPlay.substring(radioPlay.length()-1,radioPlay.length());
				radioPlay += " FM";
			}
			else
			{
				radioPlay += getAM();
				radioPlay += " AM";

			}
			radioPlay += "\n";
			System.out.println(radioPlay);
			radioPlay = "The radio is now playing ";
		}	
		public void stopRadio()
		{
			System.out.println("The Radio is off\n");
		}
	}
}