/*************************
Sean Jamison
Drexel University CS350
HW2

smj326@drexel.edu
**************************/

import java.lang.Object;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.io.Writer;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.File;
import java.io.FilenameFilter;
import java.io.BufferedReader;
import java.io.FileReader;


public class surveySystem
{
	public static void main(String[] args)
	{
		surveySystem survey = new surveySystem();
		survey.driver();
	}
	public void driver()
	{
		System.out.println("Welcome to the survey master 3000!\n\n1)Survey\n2)Test\n");
		Scanner s = new Scanner(System.in);
		String menuOption = s.nextLine();
		
			
			while(menuOption.charAt(0) != '1' && menuOption.charAt(0) != '2')
			{
				System.out.println("Invalid Menu Option please try again");
				menuOption = s.nextLine();
			}
			
			if(menuOption.charAt(0) == '1')
			{
				Survey survey = new Survey();
				menuOption = "\0";
				while(menuOption.charAt(0) != '5')
				{
					System.out.println("Survey Options:\n\n1)Create a new Survey\n2)Display a Survey\n3)Load a Survey\n4)Save a Survey\n5)Quit\n");
					menuOption = s.nextLine();
					while(menuOption.charAt(0) != '1' && menuOption.charAt(0) != '2' && menuOption.charAt(0) != '3' && menuOption.charAt(0) != '4' && menuOption.charAt(0) != '5')
					{
						System.out.println("Invalid Menu Option please try again");
						menuOption = s.nextLine();
					}
					
					if(menuOption.charAt(0) == '1')
					{
						survey.createSurvey();
					}
					else if(menuOption.charAt(0) == '2')
					{
						survey.displaySurvey();
					}
					else if(menuOption.charAt(0) == '3')
					{
						survey.loadSurvey();
					}
					else if(menuOption.charAt(0) == '4')
					{
						String filename;
						System.out.println("What do you want the file to be named (the file extension will be proveded)");
						filename = s.nextLine();
						survey.saveSurvey(filename,"surv");
					}
					else if(menuOption.charAt(0) == '5')
					{
						System.out.println("Thank you for using the Survey Master 3000!");
						System.exit(0);
					}
				}
			}
			else
			{
				Test test = new Test();
				menuOption = "\0";
				while(menuOption.charAt(0) != '5')
				{
					System.out.println("Survey Options:\n\n1)Create a new Test\n2)Display a Test\n3)Load a Test\n4)Save a Test\n5)Quit\n");
					menuOption = s.nextLine();
					while(menuOption.charAt(0) != '1' && menuOption.charAt(0) != '2' && menuOption.charAt(0) != '3' && menuOption.charAt(0) != '4' && menuOption.charAt(0) != '5')
					{
						System.out.println("Invalid Menu Option please try again");
						menuOption = s.nextLine();
					}
					if(menuOption.charAt(0) == '1')
					{
						test.createSurvey();
						test.addAnswers();
					}
					else if(menuOption.charAt(0) == '2')
					{
						System.out.println("1)Display with Answers\n2)Display without Answers");
						menuOption = s.nextLine();
						while(menuOption.charAt(0) != '1' && menuOption.charAt(0) != '2')
						{
						System.out.println("Invalid Menu Option please try again");
						menuOption = s.nextLine();
						}
						if(menuOption.charAt(0) == '1')
						{
							test.displayTest();
						}
						else
						{
							test.displaySurvey();
						}
					}
					else if(menuOption.charAt(0) == '3')
					{
						test.loadTest();
					}
					else if(menuOption.charAt(0) == '4')
					{
						String filename;
						System.out.println("What do you want the file to be named (the file extension will be proveded)");
						filename = s.nextLine();
						test.saveSurvey(filename,"test");
	
					}
					else if(menuOption.charAt(0) == '5')
					{
						System.out.println("Thank you for using the Survey Master 3000!");
						System.exit(0);
					}
				}
			}
	
		System.out.println("back to menu");
	}
	public class Survey
	{
		//Surveys are made up of lists lists of strings which are edited and then compiled
		//These lists are then turned into files that are line by line interpretations of the lists of lists
		//END and ENDANSWERS are included to specify to the system when a question ends so it can be read back in
		
		protected List<List<String>> Survey = new ArrayList<List<String>>();
		protected ArrayList<String> Question = new ArrayList<String>();
		protected String menuOptionCreation = "1";
		protected Scanner s = new Scanner(System.in);

		
		public Survey()
		{}
		public void createSurvey()
		{
			Survey = new ArrayList<List<String>>();
			Question = new ArrayList<String>();
			menuOptionCreation = "1";
			while(menuOptionCreation.charAt(0) != '2')
			{
				
				System.out.println("1) Add a new T/F question\n2) Add a new multiple choice question\n3) Add a new short answer question\n4) Add a new essay question\n5) Add a new ranking question\n");
				menuOptionCreation = s.nextLine();
				while(menuOptionCreation.charAt(0) != '1' && menuOptionCreation.charAt(0) != '2' && menuOptionCreation.charAt(0) != '3' && menuOptionCreation.charAt(0) != '4' && menuOptionCreation.charAt(0) != '5')
				{
				System.out.println("Invalid Menu Option please try again");
				menuOptionCreation = s.nextLine();
				}
				if(menuOptionCreation.charAt(0) == '1')
				{
					Question.add("tf");
				}
				else if(menuOptionCreation.charAt(0) == '2')
				{
					Question.add("mc");
				}
				else if(menuOptionCreation.charAt(0) == '3')
				{
					Question.add("sa");
				}
				else if(menuOptionCreation.charAt(0) == '4')
				{
					Question.add("es");
				}
				else if(menuOptionCreation.charAt(0) == '5')
				{
					Question.add("ranking");
				}
				System.out.println("Please input prompt\n");
				menuOptionCreation = s.nextLine();
				Question.add(menuOptionCreation);
				
				if( (Question.get(0)) != "tf" && (Question.get(0)) != "sa" && (Question.get(0)) != "es" )
				{
					System.out.println("Enter first/only answer\n");
					menuOptionCreation = s.nextLine();
					Question.add(menuOptionCreation);
					System.out.println("1) Add answer\n2) Done adding Answers\n");
					menuOptionCreation = s.nextLine();
					while(menuOptionCreation.charAt(0) != '1' && menuOptionCreation.charAt(0) != '2')
					{
						System.out.println("Improper Input please try again\n");
						menuOptionCreation = s.nextLine();	
					}
			
			
					while(menuOptionCreation.charAt(0) == '1')
					{			
						System.out.println("Enter new Answer\n");
						menuOptionCreation = s.nextLine();
						Question.add(menuOptionCreation);
						System.out.println("1) Add answer\n2) Done adding Answers\n");
						menuOptionCreation = s.nextLine();
						while(menuOptionCreation.charAt(0) != '1' && menuOptionCreation.charAt(0) != '2')
						{
							System.out.println("Improper Input please try again\n");
							menuOptionCreation = s.nextLine();	
						}
					}
				}
				if((Question.get(0)) == "tf")
				{
					Question.add("true");
					Question.add("false");					
				}
				Question.add("END");
				Survey.add(Question);
				Question = new ArrayList<String>();
				System.out.println("Would you like to add another question?\n1)Yes\n2)No");
				menuOptionCreation = s.nextLine();
				while(menuOptionCreation.charAt(0) != '1' && menuOptionCreation.charAt(0) != '2')
					{
						System.out.println("Improper Input please try again\n");
						menuOptionCreation = s.nextLine();	
					}
			}
		}
		public void displaySurvey()
		{
			if(Survey.isEmpty())
			{
				System.out.println("No survey currently loaded");
			}
			else
			{
				System.out.println("Currently loaded Survey:\n");
				int i = 2;
				for(int j = 0; j< Survey.size(); j++)
				{
					i = 2;
					System.out.println(Survey.get(j).get(1)+"?");
					if(Survey.get(j).get(0) != "es" && Survey.get(j).get(0) != "sa")
					{
						
						while (!Survey.get(j).get(i).equals("END"))
						{
							System.out.println(i + ") " + Survey.get(j).get(i));
							i++;
						}
						System.out.println("\n");
					} 
				}
			}
		}
		public void saveSurvey(String filename,String testSurvey)
		{
			if(Survey.isEmpty())
			{
				System.out.println("No survey currently loaded");
			}
			else
			{

				try (PrintWriter writer = new PrintWriter(filename+"."+testSurvey, "UTF-8")){
				for(int j = 0; j< Survey.size(); j++)
				{
					for (int i = 0; i<Survey.get(j).size();i++)
						writer.println(Survey.get(j).get(i));
				}
				
				System.out.println("File succesffuly saved");
				}
				catch(IOException e)
				{
					System.out.println("File could not be created");
				}
			}
		}
		public void loadSurvey()
			{				
				
				File folder = new File("./");
				File[] listOfFiles = folder.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith("surv");
				}});
				int numberOfFiles = 0;
				for (int i = 0; i < listOfFiles.length; i++)
				{
					if (listOfFiles[i].isFile()) 
					{
						System.out.println((i+1)+")"+ listOfFiles[i].getName());
					} 
					numberOfFiles++;
				}
				if(numberOfFiles == 0)
					return;
				System.out.println("\nWhich File would you like to load?");
				String loadFile = s.nextLine();
				while(Character.getNumericValue(loadFile.charAt(0)) <= 0 || Character.getNumericValue(loadFile.charAt(0)) > numberOfFiles)
				{
					System.out.println("Invalid File Number");
					loadFile = s.nextLine();
				}
				
				try 
				{
					File file = new File(listOfFiles[Character.getNumericValue(loadFile.charAt(0))-1].getName());
					FileReader fileReader = new FileReader(file);
					BufferedReader bufferedReader = new BufferedReader(fileReader);
					StringBuffer stringBuffer = new StringBuffer();
					String line;
					while ((line = bufferedReader.readLine()) != null)
					{
						Question.add(line);
						if(line.equals("END"))
						{
							Survey.add(Question);
							Question = new ArrayList<String>();
						}
					}
					fileReader.close();			
				}
				
				catch (IOException e) 
				{
				System.out.println("Unable to load file");
				}
				
			}
		
	}
	
	public class Test extends Survey
	{

		public Test()
		{}
		public void addAnswers()
		{
			System.out.println(Survey.size());
			for(int j = 0; j< Survey.size(); j++)
			{
				if(Survey.get(j).get(0) == "es")
				{
					Survey.get(j).add("ESSAY");
					Survey.get(j).add("ENDANSWERS");
				}
				else
				{
					System.out.println("Question Type: "+Survey.get(j).get(0));
					System.out.println("Prompt: " + Survey.get(j).get(1));
					System.out.println("Possible Answers:");
					for(int i = 2; i<Survey.get(j).size()-1; i++)
					{
						System.out.println(i-1+") " +Survey.get(j).get(i));
					}
					String properInput = "0";
					while(properInput.charAt(0) != '1')
					{
						System.out.println("Please Input the correct answer/answers corresponding number/s\nif the correct answer for a multiple choice is 1 2 and 3 enter '123'\nif it is ranking and the correct rank of each answer in the order it's\nlisted is 4 3 2 1 enter '4321'\nIf it is short answer just type the correct word/sentence");
						menuOptionCreation = s.nextLine();
						System.out.println("Is " + menuOptionCreation + " correct for your input?\n1)Yes\n2)No, I want to re-enter the correct answer");
						properInput = s.nextLine();
						while(properInput.charAt(0) != '1' && properInput.charAt(0) != '2')
								{
									System.out.println("Improper Input please try again\n");
									properInput = s.nextLine();	
								}
					}	
					Survey.get(j).add(menuOptionCreation.toLowerCase());
					Survey.get(j).add("ENDANSWERS");
				}			
		}		
	}
	public void displayTest()
		{
			if(Survey.isEmpty())
			{
				System.out.println("No survey currently loaded");
			}
			else
			{
				System.out.println("Currently loaded Survey:\n");
				int i = 2;
				for(int j = 0; j< Survey.size(); j++)
				{
					i = 2;
					System.out.println(Survey.get(j).get(1)+"?");
					if(Survey.get(j).get(0) != "es" && Survey.get(j).get(0) != "sa")
					{
						
						while (!Survey.get(j).get(i).equals("END"))
						{
							System.out.println(i + ") " + Survey.get(j).get(i));
							i++;
						}
						System.out.println("The correct answer is: "+ Survey.get(j).get(i+1));
						System.out.println("\n");
					} 
				}
			}
		}
		public void loadTest()
			{				
				File folder = new File("./");
				File[] listOfFiles = folder.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith("test");
				}});
				int numberOfFiles = 0;
				for (int i = 0; i < listOfFiles.length; i++)
				{
					if (listOfFiles[i].isFile()) 
					{
						System.out.println((i+1)+")"+ listOfFiles[i].getName());
					} 
					numberOfFiles++;
				}
				if(numberOfFiles == 0)
					return;
				System.out.println("\nWhich File would you like to load?");
				String loadFile = s.nextLine();
				while(Character.getNumericValue(loadFile.charAt(0)) <= 0 || Character.getNumericValue(loadFile.charAt(0)) > numberOfFiles)
				{
					System.out.println("Invalid File Number");
					loadFile = s.nextLine();
				}
				
				try 
				{
					
					File file = new File(listOfFiles[Character.getNumericValue(loadFile.charAt(0))-1].getName());
					FileReader fileReader = new FileReader(file);
					BufferedReader bufferedReader = new BufferedReader(fileReader);
					StringBuffer stringBuffer = new StringBuffer();
					String line;
					while ((line = bufferedReader.readLine()) != null)
					{
						if(!line.equals(""))
						{
							System.out.println(line);
							Question.add(line);
							if(line.equals("ENDANSWERS"))
							{
								Survey.add(Question);
								Question = new ArrayList<String>();
							}
						}
					}
					fileReader.close();
				}
				catch (IOException e) 
				{
				System.out.println("Unable to load file");
				}
			}
}}