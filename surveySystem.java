/*************************
Sean Jamison
Drexel University CS350
HW3

smj326@drexel.edu
**************************/

import java.lang.Object;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.io.Writer;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.File;
import java.io.FilenameFilter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.StringTokenizer;


public class surveySystem
{
	public static void main(String[] args)
	{
		surveySystem survey = new surveySystem();
		survey.driver();
	}
	public void driver()
	{
		System.out.println("Welcome to the survey master 3000!\n\n1)Survey\n2)Test\n");
		Scanner s = new Scanner(System.in);
		String menuOption = "";
		do{
			menuOption = s.nextLine();
		}while(menuOption.isEmpty());
		
			
			while(menuOption.charAt(0) != '1' && menuOption.charAt(0) != '2')
			{
				System.out.println("Invalid Menu Option please try again");
				do{
				menuOption = s.nextLine();
				}while(menuOption.isEmpty());
			}
			
			if(menuOption.charAt(0) == '1')
			{
				Survey survey = new Survey();
				menuOption = "\0";
				while(menuOption.charAt(0) != '8')
				{
					System.out.println("Survey Options:\n\n1)Create a new Survey\n2)Display a Survey\n3)Load a Survey\n4)Save a Survey\n5)Modify an Existing Survey\n6)Take a Survey\n7)Tabulate a Survey\n8)Quit\n");
					do{
					menuOption = s.nextLine();
					}while(menuOption.isEmpty());
					
					while(menuOption.charAt(0) != '1' && menuOption.charAt(0) != '2' && menuOption.charAt(0) != '3' && menuOption.charAt(0) != '4' && menuOption.charAt(0) != '5' && menuOption.charAt(0) != '6' && menuOption.charAt(0) != '7' && menuOption.charAt(0) != '8')
					{
						System.out.println("Invalid Menu Option please try again");
						do{
						menuOption = s.nextLine();
						}while(menuOption.isEmpty());
					}
					
					if(menuOption.charAt(0) == '1')
					{
						survey.createSurvey();
					}
					else if(menuOption.charAt(0) == '2')
					{
						survey.displaySurvey();
					}
					else if(menuOption.charAt(0) == '3')
					{
						survey = new Survey();
						survey.loadSurvey();
					}
					else if(menuOption.charAt(0) == '4')
					{
						String filename = "";
						System.out.println("What do you want the file to be named (the file extension will be provided)");
						do{
						filename = s.nextLine();
						}while(filename.isEmpty());
						survey.saveSurvey(filename,"surv");
					}
					else if(menuOption.charAt(0) == '5')
					{
						survey.modifySurvey("surv");
					}
					else if(menuOption.charAt(0) == '6')
					{
						String fileName = survey.loadSurvey();
						survey.takeSurveyTest("surv",fileName);
						survey = new Survey();
					}
					else if(menuOption.charAt(0) == '7')
					{
						//survey.tabulate("surv");
						System.exit(0);
					}
					else if(menuOption.charAt(0) == '8')
					{
						System.out.println("Thank you for using the Survey Master 3000!");
						System.exit(0);
					}
				}
			}
			else
			{
				Test test = new Test();
				menuOption = "\0";
				while(menuOption.charAt(0) != '9')
				{
					System.out.println("Survey Options:\n\n1)Create a new Test\n2)Display a Test\n3)Load a Test\n4)Save a Test\n5)Modify an Existing Test\n6)Take a Test\n7)Tabulate a Test\n8)Grade a Test\n9)Quit\n");
					do{
					menuOption = s.nextLine();
					}while(menuOption.isEmpty());
					while(menuOption.charAt(0) != '1' && menuOption.charAt(0) != '2' && menuOption.charAt(0) != '3' && menuOption.charAt(0) != '4' && menuOption.charAt(0) != '5' && menuOption.charAt(0) != '6' && menuOption.charAt(0) != '7' && menuOption.charAt(0) != '8' && menuOption.charAt(0) != '9')
					{
						System.out.println("Invalid Menu Option please try again");
						do{
						menuOption = s.nextLine();
						}while(menuOption.isEmpty());	
					}
					if(menuOption.charAt(0) == '1')
					{
						test.createSurvey();
						test.addAnswers();
					}
					else if(menuOption.charAt(0) == '2')
					{
						System.out.println("1)Display with Answers\n2)Display without Answers");
						do{
						menuOption = s.nextLine();
						}while(menuOption.isEmpty());
						while(menuOption.charAt(0) != '1' && menuOption.charAt(0) != '2')
						{
						System.out.println("Invalid Menu Option please try again");
						do{
						menuOption = s.nextLine();
						}while(menuOption.isEmpty());				
					}
						if(menuOption.charAt(0) == '1')
						{
							test.displayTest();
						}
						else
						{
							test.displaySurvey();
						}
					}
					else if(menuOption.charAt(0) == '3')
					{
						test = new Test();
						test.loadTest();
					}
					else if(menuOption.charAt(0) == '4')
					{
						String filename;
						System.out.println("What do you want the file to be named (the file extension will be provided)");
						do{
						filename = s.nextLine();
						}while(filename.isEmpty());	
					test.saveSurvey(filename,"test");
	
					}
					else if(menuOption.charAt(0) == '5')
					{
						String fn = test.loadTest();
						test.modifyTest(fn);
					}
					else if(menuOption.charAt(0) == '6')
					{
						String fileName = test.loadTest();
						test.takeSurveyTest("test",fileName);
						test = new Test();
					}
					else if(menuOption.charAt(0) == '7')
					{
						//test.tabulate("test");
						System.exit(0);
					}
					else if(menuOption.charAt(0) == '8')
					{
						System.out.println("Choose which Answer key will be used");
						String fn = test.loadTest();
						if(fn.isEmpty())
						{
							System.out.println("There are no Tests to be loaded");
						}
						else{
						test.gradeTest();
						System.exit(0);
						}
					}
					else if(menuOption.charAt(0) == '9')
					{
						System.out.println("Thank you for using the Survey Master 3000!");
						System.exit(0);
					}
				}
			}
	
		System.out.println("back to menu");
	}
	public class Survey
	{
		//Surveys are made up of lists lists of strings which are edited and then compiled
		//These lists are then turned into files that are line by line interpretations of the lists of lists
		//END and ENDANSWERS are included to specify to the system when a question ends so it can be read back in
		
		protected List<List<String>> Survey = new ArrayList<List<String>>();
		protected ArrayList<String> Question = new ArrayList<String>();
		protected ArrayList<String> Answer = new ArrayList<String>();
		protected String menuOptionCreation = "1";
		protected Scanner s = new Scanner(System.in);

		
		public Survey()
		{}
		public void createSurvey()
		{
			Survey = new ArrayList<List<String>>();
			Question = new ArrayList<String>();
			menuOptionCreation = "1";
			while(menuOptionCreation.charAt(0) != '2')
			{
				
				System.out.println("1) Add a new T/F question\n2) Add a new multiple choice question\n3) Add a new short answer question\n4) Add a new essay question\n5) Add a new ranking question\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());	
				while(menuOptionCreation.charAt(0) != '1' && menuOptionCreation.charAt(0) != '2' && menuOptionCreation.charAt(0) != '3' && menuOptionCreation.charAt(0) != '4' && menuOptionCreation.charAt(0) != '5')
				{
				System.out.println("Invalid Menu Option please try again");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				}
				if(menuOptionCreation.charAt(0) == '1')
				{
					Question.add("tf");
				}
				else if(menuOptionCreation.charAt(0) == '2')
				{
					Question.add("mc");
				}
				else if(menuOptionCreation.charAt(0) == '3')
				{
					Question.add("sa");
				}
				else if(menuOptionCreation.charAt(0) == '4')
				{
					Question.add("es");
				}
				else if(menuOptionCreation.charAt(0) == '5')
				{
					Question.add("ranking");
				}
				System.out.println("Please input prompt\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				Question.add(menuOptionCreation);
				
				if( (Question.get(0)) != "tf" && (Question.get(0)) != "sa" && (Question.get(0)) != "es" )
				{
					System.out.println("Enter first/only answer\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				Question.add(menuOptionCreation);
					System.out.println("1) Add answer\n2) Done adding Answers\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				while(menuOptionCreation.charAt(0) != '1' && menuOptionCreation.charAt(0) != '2')
					{
						System.out.println("Improper Input please try again\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				}
			
			
					while(menuOptionCreation.charAt(0) == '1')
					{			
						System.out.println("Enter new Answer\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				Question.add(menuOptionCreation);
						System.out.println("1) Add answer\n2) Done adding Answers\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				while(menuOptionCreation.charAt(0) != '1' && menuOptionCreation.charAt(0) != '2')
						{
							System.out.println("Improper Input please try again\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				}
					}
				}
				if((Question.get(0)) == "tf")
				{
					Question.add("true");
					Question.add("false");					
				}
				Question.add("END");
				Survey.add(Question);
				Question = new ArrayList<String>();
				System.out.println("Would you like to add another question?\n1)Yes\n2)No");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				while(menuOptionCreation.charAt(0) != '1' && menuOptionCreation.charAt(0) != '2')
					{
						System.out.println("Improper Input please try again\n");
					do{
					menuOptionCreation = s.nextLine();
					}while(menuOptionCreation.isEmpty());
				}
			}
		}
		public void displaySurvey()
		{
			if(Survey.isEmpty())
			{
				System.out.println("No survey currently loaded");
			}
			else
			{
				System.out.println("Currently loaded Survey:\n");
				int i = 2;
				for(int j = 0; j< Survey.size(); j++)
				{
					i = 2;
					System.out.println(Survey.get(j).get(1)+"?");
					if(Survey.get(j).get(0) != "es" && Survey.get(j).get(0) != "sa")
					{
						
						while (!Survey.get(j).get(i).equals("END"))
						{
							System.out.println(i + ") " + Survey.get(j).get(i));
							i++;
						}
						System.out.println("\n");
					} 
				}
			}
		}
		public void saveSurvey(String filename,String testSurvey)
		{
			if(Survey.isEmpty())
			{
				System.out.println("No survey currently loaded");
			}
			else
			{

				try (PrintWriter writer = new PrintWriter(filename+"."+testSurvey, "UTF-8")){
				for(int j = 0; j< Survey.size(); j++)
				{
					for (int i = 0; i<Survey.get(j).size();i++)
						writer.println(Survey.get(j).get(i));
				}
				
				System.out.println("File successfuly saved");
				}
				catch(IOException e)
				{
					System.out.println("File could not be created");
				}
			}
		}
		public String loadSurvey()
			{				
				String filenameFinal = "";
				File folder = new File("./");
				File[] listOfFiles = folder.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith("surv");
				}});
				
				
				int numberOfFiles = 0;
				for (int i = 0; i < listOfFiles.length; i++)
				{
					if (listOfFiles[i].isFile()) 
					{
						System.out.println((i+1)+")"+ listOfFiles[i].getName());
					} 
					numberOfFiles++;
				}
				if(numberOfFiles == 0)
				{
					System.out.println("There are no Surveys to load you must create one first");
					System.exit(0);
				}
				
				System.out.println("\nWhich File would you like to load?");
				String loadFile = "";
					do{
					loadFile = s.nextLine();
					}while(loadFile.isEmpty());	
				while(Character.getNumericValue(loadFile.charAt(0)) <= 0 || Character.getNumericValue(loadFile.charAt(0)) > numberOfFiles)
				{
					System.out.println("Invalid File Number");
					do{
					loadFile = s.nextLine();
					}while(loadFile.isEmpty());	
				}
				
				try 
				{
					filenameFinal = listOfFiles[Character.getNumericValue(loadFile.charAt(0))-1].getName();
					File file = new File(listOfFiles[Character.getNumericValue(loadFile.charAt(0))-1].getName());
					FileReader fileReader = new FileReader(file);
					BufferedReader bufferedReader = new BufferedReader(fileReader);
					StringBuffer stringBuffer = new StringBuffer();
					String line;
					while ((line = bufferedReader.readLine()) != null)
					{
						Question.add(line);
						if(line.equals("END"))
						{
							Survey.add(Question);
							Question = new ArrayList<String>();
						}
					}
					fileReader.close();			
				}
				
				catch (IOException e) 
				{
				System.out.println("Unable to load file");
				}
			return filenameFinal;	
			}
		public void modifySurvey(String testSurvey)
			{
				System.out.println("What survey do you wish to modify? (Please include file extension)");
				String fileName = "";
				String loadFile = "";
				while(! loadFile.equals("BACK"))
				{
					do{
					loadFile = s.nextLine();
					}while(loadFile.isEmpty());	
				if(! loadFile.equals("BACK"))
					{
						try 
						{
							File file = new File(loadFile);
							FileReader fileReader = new FileReader(file);
							BufferedReader bufferedReader = new BufferedReader(fileReader);
							StringBuffer stringBuffer = new StringBuffer();
							String line;
							while ((line = bufferedReader.readLine()) != null)
							{
								Question.add(line);
								if(line.equals("END"))
								{
									Survey.add(Question);
									Question = new ArrayList<String>();
								}
							}
							fileReader.close();	
							fileName = loadFile;
							loadFile = "BACK";
							
						}
						catch (IOException e) 
						{
						System.out.println("Unable to load file\nEnter Valid File or 'BACK'");
						}
					}
				}
				String Menu = "";
				int MenuOpt = -1;
					System.out.println("What question number do you wish to modify? (there are "+ Survey.size() + " Questions) \nInput 'BACK' at any time to finish editing.");
				do{
				Menu = s.nextLine();
				}while(Menu.isEmpty());
			while(! Menu.equals("BACK") )
				{
					try
					{
					MenuOpt = Integer.parseInt(Menu); 
					}
					catch(NumberFormatException e)
					{
						System.out.println("Invalid Question Number");
					}
					if(MenuOpt > 0 && MenuOpt <= Survey.size())
					{
						Survey.set(MenuOpt-1,modifySurveyQuestion(Survey.get(MenuOpt-1),1));
						
							System.out.println("Enter new filename (omit the file extension)");
							do{
							fileName = s.nextLine();
							}while(fileName.isEmpty());
							
						saveSurvey(fileName,testSurvey);
						Menu = "BACK";

					}
					else
					{
						System.out.println("Invalid Question Number");
					}
					System.out.println("What question do you wish to modify?\nInput 'BACK' at any time to finish editing.");
					do{
					Menu = s.nextLine();
					}while(Menu.isEmpty());	
				}

			}
			public List<String> modifySurveyQuestion(List<String> editQ,int surveyTest)
			{
				String finalMenu = "0";
				int MenuOpt = -1;
				String menuSelect = "0";
				String editQStr = "";
				System.out.println("Would you like to edit the prompt?\n1)Yes\n2)No");
				while(menuSelect.charAt(0) != '1' && menuSelect.charAt(0) != '2')
				{
					do{
					menuSelect = s.nextLine();
					}while(menuSelect.isEmpty());		
				if(menuSelect.charAt(0) == '1')
					{
						System.out.println(editQ.get(1)+"\nEnter a new Prompt:");
						do{
						editQStr = s.nextLine();
						}while(editQStr.isEmpty());
						editQ.set(1,editQStr);
					}
				}
				if(editQ.size() > 3)
				{
				menuSelect = "0";
				System.out.println("Would you like to edit the choices?\n1)Yes\n2)No");
				while(menuSelect.charAt(0) != '1' && menuSelect.charAt(0) != '2')
				{
						do{
						menuSelect = s.nextLine();
						}while(menuSelect.isEmpty());
					if(menuSelect.charAt(0) == '1')
					{
						while(! (finalMenu.charAt(0) == '2'))
						{
						System.out.println("Enter Which option you would like to change");
						for(int i = 2; i<editQ.size()-surveyTest; i++)
						{
							System.out.println((i-1) + ") " + editQ.get(i));
						}
						MenuOpt = -1;
						while(MenuOpt < 0 || MenuOpt >= (editQ.size() - surveyTest -1))
						{
						do{
						menuSelect = s.nextLine();
						}while(menuSelect.isEmpty());
							try
							{
							MenuOpt = Integer.parseInt(menuSelect); 
							}
							catch(NumberFormatException e)
							{
								System.out.println("Invalid Question Number");
							}
							System.out.println("Invalid Question Number");
						}
						System.out.println("Enter new Value");
						do{
						editQStr = s.nextLine();
						}while(editQStr.isEmpty());
						editQ.set(MenuOpt+1,editQStr);
						MenuOpt = -1;
						System.out.println("Would you like to change another answer\n1)Yes\n2)No");
						do{
						finalMenu = s.nextLine();
						}while(finalMenu.isEmpty());
					while(finalMenu.charAt(0) != '1' && finalMenu.charAt(0) != '2')
						{
							System.out.println("Incorrect Input");
						do{
						finalMenu = s.nextLine();
						}while(finalMenu.isEmpty());
					}
						}
					}

				}				
				}
				return editQ;
			}
		
		public void takeSurveyTest(String testSurvey,String filename)
		{
			int testSurv = 1;
			if(testSurvey.equals("test"))
			{
				testSurv = 3;
			}
			System.out.println("What is your name?");
			Answer.add(s.nextLine());
			for(int i = 0; i<= Survey.size()-1; i++)
			{
				System.out.println(Survey.get(i).get(1)+"?");
				if(Survey.get(i).get(0).equals("tf"))
				{
					System.out.println("1)True\n2)False");
				}
				else if(Survey.get(i).get(0).equals("mc"))
				{
					System.out.println("If more than one apply enter all answers e.g. if 1 and 3 are right enter '1 3'");
					for(int j = 2; j<Survey.get(i).size()-testSurv; j++)
					{
						System.out.println(j-1+")"+Survey.get(i).get(j));
					}
				}
				else if(Survey.get(i).get(0).equals("ranking"))
				{
					System.out.println("Rank all in the order they appear: e.g. if the first option is 2 and second option is 1 enter '2 1'");
					for(int j = 2; j<Survey.get(i).size()-testSurv; j++)
					{
						System.out.println(j-1+")"+Survey.get(i).get(j));
					}
				}
				Answer.add(s.nextLine());
			}
			try (PrintWriter writer = new PrintWriter(filename+"-"+Answer.get(0)+"."+testSurvey+"Resp", "UTF-8")){
				for(int i = 0; i< Answer.size(); i++)
				{
						writer.println(Answer.get(i));
				}
				
				System.out.println("File successfuly saved");
				}
				catch(IOException e)
				{
					System.out.println("File could not be created");
				}
		}
		
	}
	
	public class Test extends Survey
	{

		public Test()
		{}
		public void addAnswers()
		{
			System.out.println(Survey.size());
			for(int j = 0; j< Survey.size(); j++)
			{
				if(Survey.get(j).get(0) == "es")
				{
					Survey.get(j).add("ESSAY");
					Survey.get(j).add("ENDANSWERS");
				}
				else
				{
					System.out.println("Question Type: "+Survey.get(j).get(0));
					System.out.println("Prompt: " + Survey.get(j).get(1));
					System.out.println("Possible Answers:");
					for(int i = 2; i<Survey.get(j).size()-1; i++)
					{
						System.out.println(i-1+") " +Survey.get(j).get(i));
					}
					String properInput = "0";
					while(properInput.charAt(0) != '1')
					{
						System.out.println("Please Input the correct answer/answers corresponding number/s\nif the correct answer for a multiple choice is 1 2 and 3 enter '123'\nif it is ranking and the correct rank of each answer in the order it's\nlisted is 4 3 2 1 enter '4321'\nIf it is short answer just type the correct word/sentence");
						do{
						menuOptionCreation = s.nextLine();
						}while(menuOptionCreation=="");
						System.out.println("Is " + menuOptionCreation + " correct for your input?\n1)Yes\n2)No, I want to re-enter the correct answer");
						do{
						properInput = s.nextLine();
						}while(properInput.isEmpty());	
					while(properInput.charAt(0) != '1' && properInput.charAt(0) != '2')
								{
									System.out.println("Improper Input please try again\n");
										do{
										properInput = s.nextLine();
										}while(properInput.isEmpty());			
									}
					}	
					Survey.get(j).add(menuOptionCreation.toLowerCase());
					Survey.get(j).add("ENDANSWERS");
				}			
		}		
	}
	public void displayTest()
		{
			if(Survey.isEmpty())
			{
				System.out.println("No survey currently loaded");
			}
			else
			{
				System.out.println("Currently loaded Survey:\n");
				int i = 2;
				for(int j = 0; j< Survey.size(); j++)
				{
					i = 2;
					System.out.println(Survey.get(j).get(1)+"?");
					if(Survey.get(j).get(0) != "es" && Survey.get(j).get(0) != "sa")
					{
						
						while (!Survey.get(j).get(i).equals("END"))
						{
							System.out.println(i + ") " + Survey.get(j).get(i));
							i++;
						}
						System.out.println("The correct answer is: "+ Survey.get(j).get(i+1));
						System.out.println("\n");
					} 
				}
			}
		}
		public String loadTest()
			{				
				String filenameFinal = "";
				File folder = new File("./");
				File[] listOfFiles = folder.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith("test");
				}});
				int numberOfFiles = 0;
				for (int i = 0; i < listOfFiles.length; i++)
				{
					if (listOfFiles[i].isFile()) 
					{
						System.out.println((i+1)+")"+ listOfFiles[i].getName());
					} 
					numberOfFiles++;
				}
				if(numberOfFiles == 0)
					return "";
				System.out.println("\nWhich File would you like to load?");
				String loadFile = "";
				do{
				loadFile = s.nextLine();
				}while(loadFile.isEmpty());	
			while(Character.getNumericValue(loadFile.charAt(0)) <= 0 || Character.getNumericValue(loadFile.charAt(0)) > numberOfFiles)
				{
					System.out.println("Invalid File Number");
					do{
					loadFile = s.nextLine();
					}while(loadFile.isEmpty());				}
				
				try 
				{
					filenameFinal = listOfFiles[Character.getNumericValue(loadFile.charAt(0))-1].getName();
					File file = new File(listOfFiles[Character.getNumericValue(loadFile.charAt(0))-1].getName());
					FileReader fileReader = new FileReader(file);
					BufferedReader bufferedReader = new BufferedReader(fileReader);
					StringBuffer stringBuffer = new StringBuffer();
					String line;
					while ((line = bufferedReader.readLine()) != null)
					{
						if(!line.equals(""))
						{
							Question.add(line);
							if(line.equals("ENDANSWERS"))
							{
								Survey.add(Question);
								Question = new ArrayList<String>();
							}
						}
					}
					fileReader.close();
				}
				catch (IOException e) 
				{
				System.out.println("Unable to load file");
				}
				return filenameFinal;
			}
			
		public void modifyTest(String fileName)
			{
				
				String Menu = "";
				String ansStr = "";
				String Answers = "";
				String newAns = "";
				int MenuOpt = -1;
					System.out.println("What question number do you wish to modify? (there are "+ Survey.size() + " Questions) \nInput 'BACK' at any time to finish editing.");
				do{
				Menu = s.nextLine();
				}while(Menu.isEmpty());
			while(! Menu.equals("BACK") )
				{
					try
					{
					MenuOpt = Integer.parseInt(Menu); 
					}
					catch(NumberFormatException e)
					{
						System.out.println("Invalid Question Number");
					}
					if(MenuOpt > 0 && MenuOpt <= Survey.size())
					{
						int beginAnswers = 0;
						Survey.set(MenuOpt-1,modifySurveyQuestion(Survey.get(MenuOpt-1),3));
						System.out.println("Would you like to modify the correct Answers?\n1)Yes\n2)No");
						do{
						Menu = s.nextLine();
						}while(Menu.isEmpty());
						if(Menu.charAt(0) == '1')
						{
							System.out.println("Which Question, there are " + Survey.size());
							MenuOpt = -1;
							while(MenuOpt <= 0 || MenuOpt > Survey.size())
							{
							Menu = "";
							do{
							Menu = s.nextLine();
							}while(Menu.isEmpty());
							
							try
							{
							MenuOpt = Integer.parseInt(Menu); 
							}
							catch(NumberFormatException e)
							{
						System.out.println("Invalid Question Number");
							}
							if(MenuOpt <= 0 || MenuOpt > Survey.size())
							{
							System.out.println("Invalid Question Number Enter a new Number");
							}
							}
								System.out.println("This is the question as is with the listed answers followed by the listed correct answer");
								for(int i = 1; i< Survey.get(MenuOpt-1).size()-3; i++)
								{
									System.out.println(Survey.get(MenuOpt-1).get(i));
								}
								System.out.println(Survey.get(MenuOpt-1).get(Survey.get(MenuOpt-1).size()-2));
								System.out.println("Please Enter new Answer or press enter to leave unedited");
								newAns = s.nextLine();
								if(!newAns.isEmpty())
								{
									Survey.get(MenuOpt-1).set(Survey.get(MenuOpt-1).size()-2,newAns);
								}							
						}
							System.out.println("Enter new filename (omit the file extension)");
							do{
							fileName = s.nextLine();
							}while(fileName.isEmpty());	
								
						saveSurvey(fileName,"test");
						Menu = "BACK";
					}
					else
					{
						System.out.println("Invalid Question Number");
					}
					System.out.println("What question do you wish to modify?\nInput 'BACK' at any time to finish editing.");
					do{
					Menu = s.nextLine();
					}while(Menu.isEmpty());	
				}

			}
			public void gradeTest()
			{
				String name = "";
				int score = 0;
				ArrayList<String> Answers = new ArrayList<String>();
				ArrayList<String> answerKey = new ArrayList<String>();

				System.out.println("Choose a Response to Grade");
				String filenameFinal = "";
				File folder = new File("./");
				File[] listOfFiles = folder.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
				return name.endsWith("test-Resp");
				}});
				int numberOfFiles = 0;
				for (int i = 0; i < listOfFiles.length; i++)
				{
					if (listOfFiles[i].isFile()) 
					{
						System.out.println((i+1)+")"+ listOfFiles[i].getName());
					} 
					numberOfFiles++;
				}
				if(numberOfFiles != 0)
				{
					System.out.println("\nWhich File would you like to load?");
					String loadFile = "";
					do{
					loadFile = s.nextLine();
					}while(loadFile.isEmpty());	
				while(Character.getNumericValue(loadFile.charAt(0)) <= 0 || Character.getNumericValue(loadFile.charAt(0)) > numberOfFiles)
					{
						System.out.println("Invalid File Number");
						do{
						loadFile = s.nextLine();
						}while(loadFile.isEmpty());				}	
					try 
					{
						filenameFinal = listOfFiles[Character.getNumericValue(loadFile.charAt(0))-1].getName();
						File file = new File(listOfFiles[Character.getNumericValue(loadFile.charAt(0))-1].getName());
						FileReader fileReader = new FileReader(file);
						BufferedReader bufferedReader = new BufferedReader(fileReader);
						StringBuffer stringBuffer = new StringBuffer();
						String line;
						while ((line = bufferedReader.readLine()) != null)
						{
						Answers.add(line);
						}
					}
					catch (IOException e) 
					{
					System.out.println("Unable to load file");
					}
					for(int i = 0; i<= Survey.size()-1; i++)
					{
						answerKey.add(Survey.get(i).get(Survey.get(i).size()-2));
					}
					name = Answers.get(0);
					Answers.remove(0);
					for(int i = 0; i< answerKey.size(); i++)
					{
						if(!answerKey.get(i).equals("ESSAY"))
						{
							if(Answers.get(i).toLowerCase().replaceAll(" ", "").replaceAll("\n", "").equals(answerKey.get(i).toLowerCase().replaceAll(" ", "").replaceAll("\n", "")))
							{
								score += 10;
							};
						}
					}
					System.out.println(name + " got " + score + " / " + answerKey.size()*10 + "\nDon't forget to go grade the essay questions if there are any!");
				}
				else
				{
					System.out.println("There are no responses to grade in the current directory");
				}
			}
			

}}