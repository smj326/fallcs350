strategy seperates choice of algorithm from use of algorithm


bridge seperates abstraction from implementation

factory method - inheritance
abstract facotory  - object aggregates a factory which 


30 pts UML
56 short answer/ essay
rest points - is it a bridge

polymorphism
cohesion
abstraction
inheritance is a
composition / aggregation has a 


evil team in MLB - Red Sox



final
--------------------


question, use number of design patterns

use 3 or more design patterns 

decorator, observer and bridge
put minor ones, facade singleton and adapter could get you points back



-----------------
Final


Patterns:

Builder
	builds more complex object using simpler objects in parts
	meal
Strategy
Abstract Factory
	abstract class that holds factory methods
	polymorphism
Factory
	method that creates a class object
	polymorphism
Decorator
	adds functionality to a class after it is created
Observer  
	observes for events and then updates other objects immediately of an events
Facade
	simplifies design (tape over all buttons on remote for grandma)
Adapter
	changes an already made class to suit your own needs may be more or less complex after
Bridge
	seperates implementation from abstraction - lowers coupling
	I/O
Command
	remote control 
		also has macro ability
Iterator
	class designed to iterate through a data type (helps a parent class so it does not need to know implementation)
Template
	Tea/Coffee
	
Concepts:
	Open and close Principal
		open for extension but closed for modification
	Coupling
		loose is good - how much a class knows about another class and is dependent on it	
	Cohesion 
		high is good - a class has a single well focused purpose
	Aggregation 
		has a (diamond)
	Inheritance
		is a (hollow arrow)
	Encapsulation
		only things that need to know things get to
	Polymorphism 
		being able to handle objects of different types with their own behavior
	Abstraction 
		hide more complex details so to only show the essentials to the outside view
	Principle of encapsulation variation (if it varies encapsulate seperately)
	
Red Sox evil baseball
		






























